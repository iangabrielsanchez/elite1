@extends('layouts.app')

@section('content')
    <div class='container'>

        <a style='float:right;' href='/department/new'>Add</a>
        <h1>Departments List</h1>
        <hr />

        <ul class='list-group'>

            @foreach ($departments as $department)
                <li class="list-group-item">
                    <a href="/department/{{$department->id}}">
                        <p>
                            {{ $department->department_name}}
                        </p>
                    </a>
                </li>
            @endforeach
        </ul>

    </div>

@endsection
