<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/employees', 'EmployeesController@index');
Route::get('/departments', 'DepartmentsController@index');

Route::get('/employee/new', 'EmployeesController@create');
Route::post('/employee/new', 'EmployeesController@store');
Route::patch('/employee/{employee}/edit', "EmployeesController@update");

Route::get('/department/new', 'DepartmentsController@create');
Route::post('/department/new', 'DepartmentsController@store');
Route::patch('/department/{department}/edit', "DepartmentsController@update");

Route::get('/employee/{employee}', 'EmployeesController@view');
Route::get('/employee/{employee}/edit', "EmployeesController@edit");
Route::get('/employee/{employee}/delete', 'EmployeesController@delete');

Route::get('/department/{department}', 'DepartmentsController@view');
Route::get('/department/{department}/edit', "DepartmentsController@edit");
Route::get('/department/{department}/delete', 'DepartmentsController@delete');


Route::get('/search', 'SearchController@index');
// Route::get('/search?{searchTerm}', 'SearchController@search');
