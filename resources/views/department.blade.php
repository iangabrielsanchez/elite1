@extends('layouts.app')

@section('content')
    <div class='container'>
        <div class="panel panel-default">
            <div class="panel-heading">

                <a class="text-danger" style='float:right;' href='/department/{{$department->id}}/delete'>Delete</a>
                <p style='float:right;'>&nbsp;|&nbsp;</p>
                <a style='float:right;' href='/department/{{$department->id}}/edit'>Edit</a>

                <h2>{{ $department->department_name }}</h2>
            </div>
            <?php
                $employees = App\Employee::where('department_id', $department->id)
                    ->orderBy('lastname')
                    ->get();
            ?>
            <div class="panel-body">
                <ul class='list-group'>
                    @foreach ($employees as $employee)
                        <li class="list-group-item">
                            <a href="/employee/{{$employee->id}}">
                                <p>
                                    {{ $employee->lastname }}, {{$employee->firstname}}
                                </p>
                            </a>
                        </li>
                    @endforeach

                </ul>

            </div>
        </div>

    </div>

@endsection
