@extends('layouts.app')

@section('content')

    <div class='container'>

        <h1>Edit department</h1>
        <div class='well'>
            <form class="form-horizontal" method="POST" action="/department/{{$department->id}}/edit">
                {{method_field('PATCH')}}
                {{csrf_field()}}

                <div class="form-group">
                    <label class="control-label col-sm-2" for="dname">Department Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="dname" name="dname" placeholder="Enter department name" value="{{$department->department_name}}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

@endsection
