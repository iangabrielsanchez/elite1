@extends('layouts.app')

@section('content')
    <div class='container'>
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="text-danger" style='float:right;' href='/employee/{{$employee->id}}/delete'>Delete</a>
                <p style='float:right;'>&nbsp;|&nbsp;</p>
                <a style='float:right;' href='/employee/{{$employee->id}}/edit'>Edit</a>

                <h2>{{ $employee->lastname }}, {{$employee->firstname}}</h2>

            </div>

            <div class="panel-body">
                <p>
                    <b>Position: </b> {{$employee->position}}
                </p>

                <p>
                    <b>Department:</b> {{App\Department::find($employee->department_id)->department_name}}
                </p>

                <p>
                    <b>Contact: </b>
                    {{$employee->contact}}
                </p>

                <p>
                    <b>Address: </b>
                    {{$employee->address}}
                </p>
                <p>
                    <b>Employee Since: </b>
                    {{$employee->created_at}}
                </p>
            </div>
        </div>

    </div>

@endsection
