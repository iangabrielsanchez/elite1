<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Department;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    public function index(){
        $employees = Employee::orderBy('lastname')->get();
        return view('employees', compact('employees'));
    }

    public function view(Employee $employee){
        return view('employee',compact('employee'));
    }

    public function create(){
        $departments = Department::all();
        return view('emp_add', compact('departments'));
    }

    public function store(Request $request){
        $employee= new Employee;
        $employee->firstname = $request->fname;
        $employee->lastname = $request->lname;
        $employee->department_id = $request->department;
        $employee->contact = $request->contact;
        $employee->address = $request->addr;
        $employee->position = $request->position;
        $employee->save();
        return \Redirect::to("employee/$employee->id");
    }

    public function edit(Employee $employee){
        $departments = Department::all();
        return view('emp_edit',compact('employee', 'departments'));
    }

    public function update(Request $request, Employee $employee){
        $employee->firstname = $request->fname;
        $employee->lastname = $request->lname;
        $employee->department_id = $request->department;
        $employee->contact = $request->contact;
        $employee->address = $request->addr;
        $employee->position = $request->position;
        $employee->update();

        return \Redirect::to("employee/$employee->id");
    }

    public function delete(Employee $employee){
        Employee::destroy($employee->id);
        //$posts = Post::all();
        return \Redirect::to("employees");
    }


}
