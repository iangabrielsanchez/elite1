<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    public function index(){
        $departments = Department::orderBy('department_name')->get();
        return view('departments', compact('departments'));
    }

    public function view(Department $department){
        return view('department',compact('department'));
    }

    public function create(){
        //$departments = Department::all();
        return view('dept_add');
    }

    public function store(Request $request){
        $department= new Department;
        $department->department_name = $request->dname;
        $department->save();
        return \Redirect::to("department/$department->id");
    }

    public function update(Request $request, Department $department){
        $department->department_name = $request->dname;
        $department->update();
        return \Redirect::to("department/$department->id");
    }

    public function edit(Department $department){
        return view('dept_edit',compact('department'));
    }

    public function delete(Department $department){
        Department::destroy($department->id);
        //$posts = Post::all();
        return \Redirect::to("departments");
    }

}
