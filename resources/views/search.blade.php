@extends('layouts.app')

@section('content')

    <div class="container">
        <form class="form-horizontal" method="GET" action="/search">
            {{csrf_field()}}

            <div class="form-group">
                <label class="control-label col-sm-2" for="search">Search:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="search" name="search" placeholder="Search">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </form>

        @if(isset($_GET['search']))
            <h3>Employees</h3>
            <?php
                $results = App\Employee::where('firstname','LIKE',"%".$_GET['search']."%")
                    ->orWhere('lastname','LIKE',"%".$_GET['search']."%")
                    ->orWhere('position','LIKE',"%".$_GET['search']."%")
                    ->orWhere('contact','LIKE',"%".$_GET['search']."%")
                    ->get();
            ?>
            <ul class="list-group">
                @foreach ($results as $employee)
                        <li class="list-group-item">
                            <a href="/employee/{{$employee->id}}">
                                <p>
                                    {{ $employee->lastname }}, {{$employee->firstname}}
                                </p>
                            </a>
                        </li>
                @endforeach
            </ul>
            <hr />
            <h3>Departments</h3>
            <?php
                $results = App\Department::where('department_name','LIKE',"%".$_GET['search']."%")
                    ->orWhere('id','LIKE',"%".$_GET['search']."%")
                    ->get();
            ?>
            <ul class="list-group">
                @foreach ($results as $department)
                    <li class="list-group-item">
                        <a href="/department/{{$department->id}}">
                            <p>
                                {{ $department->department_name}}
                            </p>
                        </a>
                    </li>
                @endforeach
            </ul>




        @endif

    </div>

@endsection
