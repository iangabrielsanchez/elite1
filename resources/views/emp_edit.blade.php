@extends('layouts.app')

@section('content')

    <div class='container'>

        <h1>Add new employee</h1>
        <div class='well'>
            <form class="form-horizontal" method="POST" action="/employee/{{$employee->id}}/edit">
                {{method_field('PATCH')}}
                {{csrf_field()}}

                <div class="form-group">
                    <label class="control-label col-sm-2" for="fname">First Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="fname" name="fname" placeholder="Enter first name" value="{{$employee->firstname}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="lname">Last Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter last name" value="{{$employee->lastname}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="dept">Department:</label>
                    <div class="col-sm-10">
                        <select id='dept' name='department'>
                            @foreach ($departments as $department)
                                <option value="{{$department->id}}"
                                    @if ($department->id == $employee->department_id)
                                        selected
                                    @endif
                                    >
                                    {{$department->department_name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="position">Position:</label>
                    <div class="col-sm-10">
                        <input type="tel" class="form-control" id="position" name="position" placeholder="Enter position" value="{{$employee->position}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="contact">Contact number:</label>
                    <div class="col-sm-10">
                        <input type="tel" class="form-control" id="contact" name="contact" placeholder="Enter contact number" value="{{$employee->contact}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="addr">Home Address:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="addr" name="addr" placeholder="Enter home address" value="{{$employee->address}}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

@endsection
