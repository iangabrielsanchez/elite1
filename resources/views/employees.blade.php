@extends('layouts.app')

@section('content')
    <div class='container'>

        <a style='float:right;' href='/employee/new'>Add</a>
        <h1>Employees List</h1>
        
        <hr />

        <ul class='list-group'>

            @foreach ($employees as $employee)
                <li class="list-group-item">
                    <a href="/employee/{{$employee->id}}">
                        <p>
                            {{ $employee->lastname }}, {{$employee->firstname}}
                        </p>
                    </a>
                </li>
            @endforeach
        </ul>

    </div>

@endsection
